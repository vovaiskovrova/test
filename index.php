<?php
/**
 * Created by PhpStorm.
 * User: flip
 * Date: 13.07.2021
 * Time: 9:13
 */
error_reporting(E_ERROR);
require_once('config.php');
require_once(DIR_ROOT . '/app/startup.php');

$task = new \App\Controllers\Task();
$task->route();