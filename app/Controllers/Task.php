<?php
/**
 * Created by PhpStorm.
 * User: flip
 * Date: 13.07.2021
 * Time: 9:13
 */

namespace App\Controllers;

class Task extends \App\Common
{

    public function route()
    {
        if ($_GET['action'] == 'editTask') {
            $this->getForm();
        } elseif ($_GET['action'] == 'saveTask') {
            $this->saveForm();
        } elseif ($_GET['action'] == 'loginForm') {
            $this->loginForm();
        } elseif ($_GET['action'] == 'login') {
            $this->login();
        } elseif ($_GET['action'] == 'logout') {
            $this->logout();
        } else {
            $this->getList();
        }
    }

    public function getForm()
    {
        if (isset($_GET['taskId']) && (int)$_GET['taskId']) {
            $this->tpl->addData(false, array(
                'task' => new \App\Models\Entity\Task($_GET['taskId'])
            ));
        }
        $this->tpl->display('taskForm.html');
    }

    public function saveForm()
    {
        if (isset($_GET['id']) && !$this->user->isAuth()) {
            header('Location: index.php?action=loginForm');
            exit();
        }

        $error = false;

        if (isset($_POST['name']) && $_POST['name'] == '' &&
            isset($_POST['email']) && $_POST['email'] == '' &&
            isset($_POST['text']) && $_POST['text'] == ''
        ) {
            $error = 'Введите хоть чтонибудь';
        }

        if (isset($_POST['email']) && $_POST['email'] != '' &&
            !preg_match("/^(?:[a-z0-9]+(?:[-_.]?[a-z0-9]+)?@[a-z0-9_.-]+(?:\.?[a-z0-9]+)?\.[a-z]{2,5})$/i", $_POST['email'])
        ) {
            $error = "Email указан не правильно.";
        }

        if ($error) {
            $this->tpl->addData(false, array(
                'error' => $error
            ));
            $this->getForm();
        } else {
            $task = new \App\Models\Entity\Task($_GET['id']);

            // проверка на "отредактировано администратором"
            if (isset($_GET['id']) && $_POST['text'] != $task->getText()) {
                $task->setEditAdmin();
            }

            $task
                ->setName($_POST['name'])
                ->setEmail($_POST['email'])
                ->setText($_POST['text'])
                ->setStatus($_POST['status'])
                ->save();

            $this->tpl->addData(false, array(
                'addSuccess' => true
            ));
            $this->getList();
        }
    }

    public function getList()
    {

        if ($_GET['curPage']) {
            $curPage = (int)$_GET['curPage'];
        } else {
            $curPage = 1;
        }

        if ($_GET['orderDesc'] && in_array($_GET['orderDesc'], array('ASC', 'DESC'))) {
            $orderDesc = $_GET['orderDesc'];
        } else {
            $orderDesc = 'ASC';
        }

        if ($_GET['orderField']) {
            $orderField = $_GET['orderField'];
        } else {
            $orderField = 'id';
        }

        $coll = new \App\Models\EntityCollection\TaskCollection();
        $coll
            ->setLimitStart(($curPage - 1) * LIMIT_ON_PAGE)
            ->setOrderField($orderField)
            ->setOrderDesc($orderDesc);

        $this->tpl->addData(false, array(
            'tasks'       => $coll->init(),
            'orderField'  => $orderField,
            'orderDesc'   => $orderDesc,
            'orderDescTh' => ($orderDesc == 'ASC' ? 'DESC' : 'ASC'),
            'curPage'     => $curPage,
            'user'        => $this->user,
            'countPage'   => ceil($coll->getFoundRows() / LIMIT_ON_PAGE)
        ));

        $this->tpl->display('taskList.html');
    }

    public function loginForm()
    {
        $this->tpl->display('loginForm.html');
    }

    /**
     * проверим введенные логин и пароль
     * если все норм, то поставим сессию
     */
    public function login()
    {
        if (isset($_POST['login']) && isset($_POST['pass'])) {
            if ($_POST['login'] != '' && $_POST['pass'] != '') {
                if ($_POST['login'] == ADMIN_LOGIN && $_POST['pass'] == ADMIN_PASS) {
                    if ($this->user->login()) {
                        header("Location: index.php");
                    }
                } else {
                    $this->tpl->addData(false, array(
                        'error' => 'неправильныe реквизиты доступа'
                    ));
                }
            } else {
                $this->tpl->addData(false, array(
                    'error' => 'введенные данные не верные'
                ));
            }
        }
        $this->tpl->display('loginForm.html');
    }

    public function logout()
    {
        $this->user->logout();
        header("Location: index.php");
    }
} 