<?php

/* taskForm.html */
class __TwigTemplate_0dd1fee3672da4ac36e9db86e4f620168021a2a90b5ee14bf2179414ddcb7cd4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("/base.html");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "/base.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "<form class=\"w-50\" action=\"index.php?action=saveTask";
        if ($this->getAttribute((isset($context["task"]) ? $context["task"] : null), "getId")) {
            echo "&id=";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["task"]) ? $context["task"] : null), "getId"), "html", null, true);
        }
        echo "\" method=\"post\">

    ";
        // line 6
        if ((isset($context["error"]) ? $context["error"] : null)) {
            // line 7
            echo "    <div class=\"p-3 mb-2 bg-danger text-white\">
        ";
            // line 8
            echo twig_escape_filter($this->env, (isset($context["error"]) ? $context["error"] : null), "html", null, true);
            echo "
    </div>
    ";
        }
        // line 11
        echo "
    <div class=\"form-group\">
        <label for=\"exampleFormControlInput2\">Имя</label>
        <input type=\"name\" name=\"name\" required=\"true\" value=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["task"]) ? $context["task"] : null), "getName"), "html", null, true);
        echo "\" class=\"form-control\" id=\"exampleFormControlInput2\" placeholder=\"Вася Пупкин\">
    </div>
    <div class=\"form-group\">
        <label for=\"exampleFormControlInput1\">Email</label>
        <input type=\"email\" name=\"email\" required=\"true\" value=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["task"]) ? $context["task"] : null), "getEmail"), "html", null, true);
        echo "\" class=\"form-control\" id=\"exampleFormControlInput1\" placeholder=\"name@example.com\">
    </div>
    <div class=\"form-group\">
        <label for=\"exampleFormControlTextarea1\">Текст задачи</label>
        <textarea class=\"form-control\" required=\"true\" name=\"text\" id=\"exampleFormControlTextarea1\" rows=\"3\">";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["task"]) ? $context["task"] : null), "getText"), "html", null, true);
        echo "</textarea>
    </div>
    ";
        // line 24
        if ($this->getAttribute((isset($context["task"]) ? $context["task"] : null), "getId")) {
            // line 25
            echo "    <div class=\"form-check\">
        <input class=\"form-check-input\" type=\"checkbox\" name=\"status\" value=\"1\" id=\"defaultCheck1\" ";
            // line 26
            if ($this->getAttribute((isset($context["task"]) ? $context["task"] : null), "getStatus")) {
                echo "checked";
            }
            echo ">
        <label class=\"form-check-label\" for=\"defaultCheck1\">
            Статус выполнено
        </label>
    </div>
    ";
        }
        // line 32
        echo "    <div class=\"form-group mt-3 text-right\">
        <button type=\"submit\" class=\"btn btn-info\">Сохранить</button>
    </div>
</form>
";
    }

    public function getTemplateName()
    {
        return "taskForm.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 32,  80 => 26,  77 => 25,  75 => 24,  70 => 22,  63 => 18,  56 => 14,  51 => 11,  45 => 8,  42 => 7,  40 => 6,  31 => 4,  28 => 3,);
    }
}
