<?php

/* loginForm.html */
class __TwigTemplate_661da1a1f89e50137e7c70cfa6e66836ba701a70e93b3dbcb471827911a57d10 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("/base.html");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "/base.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "<form class=\"w-25\" action=\"index.php?action=login\" method=\"post\">

    ";
        // line 6
        if ((isset($context["error"]) ? $context["error"] : null)) {
            // line 7
            echo "        <div class=\"p-3 mb-2 bg-danger text-white\">
            ";
            // line 8
            echo twig_escape_filter($this->env, (isset($context["error"]) ? $context["error"] : null), "html", null, true);
            echo "
        </div>
    ";
        }
        // line 11
        echo "
    <div class=\"form-group\">
        <label for=\"exampleFormControlInput2\">Введите логин</label>
        <input type=\"text\" name=\"login\" class=\"form-control\" id=\"exampleFormControlInput2\">
    </div>
    <div class=\"form-group\">
        <label for=\"exampleFormControlInput1\">Введите пароль</label>
        <input type=\"password\" name=\"pass\" class=\"form-control\" id=\"exampleFormControlInput1\">
    </div>
    <div class=\"form-group\">
        <button class=\"btn btn-info w-100\">Войти</button>
    </div>
</form>
";
    }

    public function getTemplateName()
    {
        return "loginForm.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  46 => 11,  40 => 8,  37 => 7,  35 => 6,  31 => 4,  28 => 3,);
    }
}
