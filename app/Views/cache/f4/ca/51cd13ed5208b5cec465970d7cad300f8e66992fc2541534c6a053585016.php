<?php

/* taskList.html */
class __TwigTemplate_f4ca51cd13ed5208b5cec465970d7cad300f8e66992fc2541534c6a053585016 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("/base.html");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "/base.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "
    <div class=\"row mx-1\">
        <div class=\"col\">
            <a href=\"index.php?action=editTask\" class=\"btn btn-success\">добавить задачу</a>
        </div>
        <div class=\"col text-right\">
            ";
        // line 10
        if ($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "isAuth")) {
            // line 11
            echo "                <a href=\"index.php?action=logout\" class=\"btn btn-warning\">Выйти</a>
            ";
        } else {
            // line 13
            echo "                <a href=\"index.php?action=loginForm\" class=\"btn btn-danger\">Войти как админ</a>
            ";
        }
        // line 15
        echo "        </div>
    </div>

        ";
        // line 18
        if ((isset($context["addSuccess"]) ? $context["addSuccess"] : null)) {
            // line 19
            echo "        <div class=\"row mx-1 my-2  text-center\">
            <div class=\"p-3 mb-2 bg-info text-white text-center\">
                Задача добавлена
            </div>
        </div>
        ";
        }
        // line 25
        echo "
    ";
        // line 26
        if ($this->getAttribute((isset($context["tasks"]) ? $context["tasks"] : null), "count")) {
            // line 27
            echo "        <table class=\"table mt-3\">
            <tr>
                <th><a href=\"index.php?orderField=name&orderDesc=";
            // line 29
            echo twig_escape_filter($this->env, (isset($context["orderDescTh"]) ? $context["orderDescTh"] : null), "html", null, true);
            echo "&curPage=";
            echo twig_escape_filter($this->env, (isset($context["curPage"]) ? $context["curPage"] : null), "html", null, true);
            echo "\">Имя</a></th>
                <th><a href=\"index.php?orderField=email&orderDesc=";
            // line 30
            echo twig_escape_filter($this->env, (isset($context["orderDescTh"]) ? $context["orderDescTh"] : null), "html", null, true);
            echo "&curPage=";
            echo twig_escape_filter($this->env, (isset($context["curPage"]) ? $context["curPage"] : null), "html", null, true);
            echo "\">email</a></th>
                <th>Текст задачи</th>
                <th><a href=\"index.php?orderField=status&orderDesc=";
            // line 32
            echo twig_escape_filter($this->env, (isset($context["orderDescTh"]) ? $context["orderDescTh"] : null), "html", null, true);
            echo "&curPage=";
            echo twig_escape_filter($this->env, (isset($context["curPage"]) ? $context["curPage"] : null), "html", null, true);
            echo "\">Статус</a></th>
                ";
            // line 33
            if ($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "isAuth")) {
                // line 34
                echo "                    <th></th>
                ";
            }
            // line 36
            echo "            </tr>
        ";
            // line 37
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["tasks"]) ? $context["tasks"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["task"]) {
                // line 38
                echo "            <tr>
                <td>";
                // line 39
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["task"]) ? $context["task"] : null), "getName"), "html", null, true);
                echo "</td>
                <td>";
                // line 40
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["task"]) ? $context["task"] : null), "getEmail"), "html", null, true);
                echo "</td>
                <td>";
                // line 41
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["task"]) ? $context["task"] : null), "getText"), "html", null, true);
                echo "</td>
                <td>
                    ";
                // line 43
                if ($this->getAttribute((isset($context["task"]) ? $context["task"] : null), "getStatus")) {
                    // line 44
                    echo "                        Выполнено
                    ";
                } else {
                    // line 46
                    echo "                        Не выполнено
                    ";
                }
                // line 48
                echo "
                    ";
                // line 49
                if ($this->getAttribute((isset($context["task"]) ? $context["task"] : null), "getEditAdmin")) {
                    // line 50
                    echo "                        <br />отредактировано администратором
                    ";
                }
                // line 52
                echo "                </td>
                ";
                // line 53
                if ($this->getAttribute((isset($context["user"]) ? $context["user"] : null), "isAuth")) {
                    // line 54
                    echo "                <td>
                    <a href=\"index.php?action=editTask&taskId=";
                    // line 55
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["task"]) ? $context["task"] : null), "getId"), "html", null, true);
                    echo "\">изменить</a>
                </td>
                ";
                }
                // line 58
                echo "
            </tr>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['task'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 61
            echo "        </table>
    ";
        }
        // line 63
        echo "

    ";
        // line 65
        if (((isset($context["countPage"]) ? $context["countPage"] : null) > 1)) {
            // line 66
            echo "        ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable(range(1, (isset($context["countPage"]) ? $context["countPage"] : null)));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 67
                echo "            ";
                if (((isset($context["i"]) ? $context["i"] : null) == (isset($context["curPage"]) ? $context["curPage"] : null))) {
                    // line 68
                    echo "                <b>";
                    echo twig_escape_filter($this->env, (isset($context["i"]) ? $context["i"] : null), "html", null, true);
                    echo "</b>
            ";
                } else {
                    // line 70
                    echo "                <a href=\"index.php?orderField=";
                    echo twig_escape_filter($this->env, (isset($context["orderField"]) ? $context["orderField"] : null), "html", null, true);
                    echo "&orderDesc=";
                    echo twig_escape_filter($this->env, (isset($context["orderDesc"]) ? $context["orderDesc"] : null), "html", null, true);
                    echo "&curPage=";
                    echo twig_escape_filter($this->env, (isset($context["i"]) ? $context["i"] : null), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, (isset($context["i"]) ? $context["i"] : null), "html", null, true);
                    echo "</a>
            ";
                }
                // line 72
                echo "        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 73
            echo "    ";
        }
        // line 74
        echo "
";
    }

    public function getTemplateName()
    {
        return "taskList.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  207 => 74,  204 => 73,  198 => 72,  186 => 70,  180 => 68,  177 => 67,  172 => 66,  170 => 65,  166 => 63,  162 => 61,  154 => 58,  148 => 55,  145 => 54,  143 => 53,  140 => 52,  136 => 50,  134 => 49,  131 => 48,  127 => 46,  123 => 44,  121 => 43,  116 => 41,  112 => 40,  108 => 39,  105 => 38,  101 => 37,  98 => 36,  94 => 34,  92 => 33,  86 => 32,  79 => 30,  73 => 29,  69 => 27,  67 => 26,  64 => 25,  56 => 19,  54 => 18,  49 => 15,  45 => 13,  41 => 11,  39 => 10,  31 => 4,  28 => 3,);
    }
}
