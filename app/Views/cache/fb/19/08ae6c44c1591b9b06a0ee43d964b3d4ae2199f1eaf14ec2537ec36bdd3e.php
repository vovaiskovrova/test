<?php

/* /base.html */
class __TwigTemplate_fb1908ae6c44c1591b9b06a0ee43d964b3d4ae2199f1eaf14ec2537ec36bdd3e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<html lang=\"en\">
<head>
    <!-- Required meta tags -->
    <meta charset=\"utf-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">

    <!-- Bootstrap CSS -->
    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css\" integrity=\"sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm\" crossorigin=\"anonymous\">

    <title>Тайтл</title>
</head>
<body>

<div class=\"m-2\">
";
        // line 16
        $this->displayBlock('content', $context, $blocks);
        // line 18
        echo "</div>

<script src=\"https://code.jquery.com/jquery-3.2.1.slim.min.js\" integrity=\"sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN\" crossorigin=\"anonymous\"></script>
<script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js\" integrity=\"sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q\" crossorigin=\"anonymous\"></script>
<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js\" integrity=\"sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl\" crossorigin=\"anonymous\"></script>
</body>
</html>";
    }

    // line 16
    public function block_content($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "/base.html";
    }

    public function getDebugInfo()
    {
        return array (  37 => 16,  20 => 1,  168 => 54,  161 => 52,  149 => 50,  143 => 48,  140 => 47,  136 => 46,  132 => 44,  128 => 42,  120 => 39,  116 => 37,  112 => 35,  110 => 34,  105 => 32,  101 => 31,  97 => 30,  93 => 29,  90 => 28,  86 => 27,  79 => 25,  72 => 23,  66 => 22,  60 => 21,  56 => 19,  54 => 18,  49 => 16,  45 => 13,  41 => 11,  39 => 18,  31 => 4,  28 => 3,);
    }
}
