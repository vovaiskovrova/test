<?php
/**
 * Created by PhpStorm.
 * User: flip
 * Date: 13.07.2021
 * Time: 11:37
 */

namespace App;


class DB {

    private $conn;
    protected static $_instance;

    public function __construct(){

        $this->conn = mysqli_connect(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD);
        if (!$this->conn ) {
            trigger_error('Error: Could not make a database link using ' . DB_USERNAME . '@' . DB_HOSTNAME);
        }

        if (!mysqli_select_db($this->conn, DB_DATABASE)) {
            trigger_error('Error: Could not connect to database ' . DB_DATABASE);
        }

        $this->q("SET NAMES 'utf8'");
        $this->q("SET CHARACTER SET utf8");
        $this->q("SET CHARACTER_SET_CONNECTION=utf8");
        $this->q("SET SQL_MODE = ''");

    }

    public function q($sql){
        return mysqli_query($this->conn, $sql);
    }

    public function fetch_array($res){
        return mysqli_fetch_array($res);
    }

    public function escape($v){
        return mysqli_real_escape_string($this->conn, $v);
    }

    public static function getInstance() {
        if (self::$_instance === null) {
            self::$_instance = new self;
        }

        return self::$_instance;
    }
} 