<?php

Class Templater extends Twig_Environment
{

    protected $data;
    protected static $_instance;

    public function __construct()
    {

        $this->data = array();

        parent::__construct(new Twig_Loader_Filesystem(DIR_ROOT . "/app/Views"),
            array(
                'cache'       => DIR_ROOT . "/app/Views/cache",
                'charset'     => 'utf-8',
                'auto_reload' => true,
            )
        );

    }

    public static function getInstance() {
        if (self::$_instance === null) {
            self::$_instance = new self;
        }

        return self::$_instance;
    }


    public function addData($index, $arr)
    {
        if ($index) {
            if (is_array($this->data[$index]) && count($this->data[$index])) {
                $this->data[$index] = array_merge($arr, $this->data[$index]);
            } else {
                $this->data[$index] = $arr;
            }
        } else {
            if (is_array($this->data) && is_array($arr)) {
                $this->data = array_merge($this->data, $arr);
            }
        }
        return $this;
    }

    protected function load($tplFile)
    {
        if (!$tplFile) {
            $class = preg_replace('#^.*/(.*?)$#', '$1', str_ireplace('\\', '/', get_class($this)));
            $tplFile = "Layout/layout{$class}.html";
        }
        return $this->loadTemplate($tplFile);
    }


    public function display($tplFile = false)
    {
        $tpl = $this->load($tplFile);
        $tpl->display($this->data);
    }

    public function render($tplFile = false)
    {
        $tpl = $this->load($tplFile);
        return $tpl->render($this->data);
    }

    public function getData($index = '')
    {
        return ($index && isset($this->data[$index])) ? $this->data[$index] : $this->data;
    }
}