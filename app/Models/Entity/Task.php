<?php
/**
 * Created by PhpStorm.
 * User: flip
 * Date: 13.07.2021
 * Time: 9:13
 */

namespace App\Models\Entity;


class Task extends \App\Common
{

    public function __construct($id = false)
    {
        parent::__construct();
        if ($id) {
            $res = $this->db->q("SELECT id, name, email, text, status, editAdmin FROM tasks where id = " . (int)$id . "");
            $this->data = $this->db->fetch_array($res);
        }
    }

    public function getId()
    {
        return $this->data['id'];
    }

    public function getName()
    {
        return $this->data['name'];
    }

    public function getEmail()
    {
        return $this->data['email'];
    }

    public function getText()
    {
        return $this->data['text'];
    }

    public function getStatus()
    {
        return $this->data['status'];
    }

    public function getEditAdmin()
    {
        return (int)$this->data['editAdmin'];
    }

    public function setName($v)
    {
        $this->data['name'] = $v;
        return $this;
    }

    public function setEmail($v)
    {
        $this->data['email'] = $v;
        return $this;
    }

    public function setText($v)
    {
        $this->data['text'] = $v;
        return $this;
    }

    public function setStatus($v)
    {
        $this->data['status'] = $v;
        return $this;
    }

    public function setEditAdmin()
    {
        $this->data['editAdmin'] = 1;
        return $this;
    }

    public function save()
    {
        $this->db->q("
                    INSERT INTO tasks
                    (
                      id
                     ,name
                     ,email
                     ,text
                     ,status
                     ,editAdmin
                    )
                    VALUES
                    (
                      " . (int)$this->data['id'] . "
                     ,'{$this->db->escape($this->data['name'])}'
                     ,'{$this->db->escape($this->data['email'])}'
                     ,'{$this->db->escape($this->data['text'])}'
                     ," . (int)$this->data['status'] . "
                     ," . (int)$this->data['editAdmin'] . "
                    ) ON DUPLICATE KEY UPDATE
                     name = '{$this->db->escape($this->data['name'])}'
                     ,email = '{$this->db->escape($this->data['email'])}'
                     ,text = '{$this->db->escape($this->data['text'])}'
                     ,status = " . (int)$this->data['status'] . "
                     ,editAdmin = " . (int)$this->data['editAdmin'] . "
                    ");

    }
} 