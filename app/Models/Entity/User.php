<?php
/**
 * Created by PhpStorm.
 * User: flip
 * Date: 13.07.2021
 * Time: 9:14
 */

namespace App\Models\Entity;


class User
{

    protected static $_instance;

    public function __construct()
    {

    }

    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self;
        }

        return self::$_instance;
    }

    public function login()
    {
        if (session_id()) {
            setcookie('userSess', md5(session_id()), time() + 60 * 60 * 24 * 30, '/', $_SERVER['HTTP_HOST']);
            return true;
        } else {
            return false;
        }
    }

    public function logout()
    {
        setcookie('userSess', '', time() - 60 * 60 * 24 * 30, '/', $_SERVER['HTTP_HOST']);
        return true;
    }

    public function isAuth()
    {
        if (isset($_COOKIE['userSess']) && $_COOKIE['userSess'] != '' && $_COOKIE['userSess'] == md5(session_id())) {
            return true;
        } else {
            return false;
        }
    }
} 