<?php
/**
 * Created by PhpStorm.
 * User: flip
 * Date: 13.07.2021
 * Time: 9:14
 */

namespace App\Models\EntityCollection;

class TaskCollection extends \App\Common implements \IteratorAggregate, \Countable
{

    private $items;
    private $orderField = 'name';
    private $orderDesc = 'ASC';
    private $limitStart = 0;

    public function __construct()
    {
        parent::__construct();
    }

    public function getIterator()
    {
        return new \ArrayIterator($this->items);
    }

    public function count()
    {
        return count($this->items);
    }

    public function setLimitStart($v)
    {
        if ($v > 0) {
            $this->limitStart = $v;
        }
        return $this;
    }

    public function setOrderField($v)
    {
        if ($v != '') {
            $this->orderField = $v;
        }
        return $this;
    }

    public function setOrderDesc($v)
    {
        if (in_array($v, array('ASC', 'DESC'))) {
            $this->orderDesc = $v;
        }
        return $this;
    }

    public function init()
    {
        $res = $this->db->q("SELECT SQL_CALC_FOUND_ROWS id
                                FROM tasks
                                ORDER BY {$this->db->escape($this->orderField)}
                                {$this->orderDesc}
                                LIMIT " . (int)$this->limitStart . ", " . LIMIT_ON_PAGE . "");

        $r = $this->db->q("SELECT FOUND_ROWS()");
        $this->foundRows = $this->db->fetch_array($r);

        while ($row = $this->db->fetch_array($res)) {
            $task = new \App\Models\Entity\Task($row['id']);
            $this->items[$task->getId()] = $task;
        }
        return $this;
    }

    public function getFoundRows()
    {
        return $this->foundRows[0];
    }

} 