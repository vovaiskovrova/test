<?php
namespace App;

Class Common{

    public function __construct(){
        $this->tpl = \Templater::getInstance();
        $this->db = \App\DB::getInstance();
        $this->user = \App\Models\Entity\User::getInstance();
    }

}