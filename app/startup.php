<?php
/**
 * Created by PhpStorm.
 * User: flip
 * Date: 13.07.2021
 * Time: 9:26
 */

session_start();

require_once(DIR_ROOT . "/app/Vendor/Twig/Autoloader.php");
Twig_Autoloader::register();
require_once(DIR_ROOT . "/app/Templater.php");

require DIR_ROOT . "/app/Vendor/Zend/Loader/StandardAutoloader.php";
$autoLoader = new Zend\Loader\StandardAutoloader(array('namespaces' => array('App' => DIR_ROOT . "/app",
                                                                             'Zend' => DIR_ROOT . "/app/Vendor/Zend"),
                                                       'fallback_autoloader' => false));
$autoLoader->register();