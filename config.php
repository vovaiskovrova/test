<?php
/**
 * Created by PhpStorm.
 * User: flip
 * Date: 13.07.2021
 * Time: 9:13
 */

define('DIR_ROOT', dirname(__FILE__));

define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '123');
define('DB_DATABASE', 'tasks');

define('LIMIT_ON_PAGE', 3);

define('ADMIN_LOGIN', 'admin');
define('ADMIN_PASS', '123');