CREATE TABLE tasks (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(50) DEFAULT NULL,
  email varchar(50) DEFAULT NULL,
  text varchar(255) DEFAULT NULL,
  status tinyint(4) DEFAULT 0,
  editAdmin tinyint(4) DEFAULT 0,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 4096,
CHARACTER SET utf8,
COLLATE utf8_general_ci;